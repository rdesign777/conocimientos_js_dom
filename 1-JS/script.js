"use strict";

const randomNum = () => {
    const rand = Math.floor(Math.random() * 100);
    console.log(rand)
    return rand;
};

const askInput = ( tries ) => Number.parseInt( prompt( `Enter a number between 0 and 100.
You have ${ tries } ${ tries === 1 ? 'try' : 'tries' } left.` ) );

const validateAnswer = ( randomInteger, userInput, tries ) => {
    if( userInput !== randomInteger ){
        if ( tries > 0) {
            alert(`Too ${ userInput > randomInteger ? 'high' : 'low'}. Try again... ${ tries } ${ tries === 1 ? 'try' : 'tries '}`)
        }
        return false;
    }
    return true;
};

const randomNumber = randomNum();
let isCorrect = false;
for( let i = 5; i > 0; i-- ){
    console.log(i);
    const userInput = askInput( i );
    isCorrect = validateAnswer( randomNumber, userInput, i - 1 );
    if( isCorrect ){
        break;
    }
}

( isCorrect ) ? alert( `You got it!` ) : alert( `Try again later` );